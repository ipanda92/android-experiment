package com.example.work1;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    String user_sex_info = "";
    String user_edu_info = "";
    String user_habit1 = "";
    String user_habit2 = "";
    String user_habit3 = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Log.i("TAG", "program star");
        RadioGroup radgroup = findViewById(R.id.choose_sex);
        CheckBox habit1 = findViewById(R.id.checkBox);
        CheckBox habit2 = findViewById(R.id.checkBox2);
        CheckBox habit3 = findViewById(R.id.checkBox3);

        habit1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Spinner edu_spinner = findViewById(R.id.spinner);
                String edu_str = edu_spinner.getSelectedItem().toString();
                Log.i("TAG", ":爱好选项框被选择" );
                if (user_habit1.length() == 0){
                    user_habit1 = "游泳、";
                }
                else {
                    user_habit1 = "";
                }
            }
        });

        habit2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Spinner edu_spinner = findViewById(R.id.spinner);
                String edu_str = edu_spinner.getSelectedItem().toString();
                Log.i("TAG", ":爱好选项框被选择" );
                if (user_habit2.length() == 0){
                    user_habit2 = "音乐、";
                }
                else {
                    user_habit2 = "";
                }
            }
        });

        habit3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Spinner edu_spinner = findViewById(R.id.spinner);
                String edu_str = edu_spinner.getSelectedItem().toString();
                Log.i("TAG", ":爱好选项框被选择" );
                if (user_habit3.length() == 0){
                    user_habit3 = "阅读、";
                }
                else {
                    user_habit3 = "";
                }
            }
        });



        //第一种获得单选按钮值的方法
        //为radioGroup设置一个监听器:setOnCheckedChanged()
        radgroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radbtn = findViewById(checkedId);
                user_sex_info = radbtn.getText().toString();
            }
        });

//        注册按钮监听器
        Button reg_btn = findViewById(R.id.button);
        reg_btn.setOnClickListener(view -> {
            Spinner spinner = findViewById(R.id.spinner);
            user_edu_info = spinner.getSelectedItem().toString();
            TextView textView = findViewById(R.id.textView5);
            textView.setText(user_sex_info + "  " + user_edu_info + "  " + user_habit1 + user_habit2 + user_habit3);

            final String[] city = new String[]{"男", "女"};
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("选择你的性别");
            builder.setSingleChoiceItems(city, 0, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Toast.makeText(getApplicationContext(), "你选择了" + city[which], Toast.LENGTH_SHORT).show();
                }
            });
            AlertDialog alert = builder.create();
            alert.show();
        } );




    }
}